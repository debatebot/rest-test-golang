package main

import (
	"os"
	"log"
	"testing"
	_ "database/sql"
	_ "fmt"
	"net/http"
	"net/http/httptest"
	"encoding/json"
	"bytes"
	"fmt"
	"encoding/csv"
	"strconv"

)

var a App

const createTable = `
CREATE TABLE IF NOT EXISTS users
(
	id SERIAL PRIMARY KEY,
	first_name VARCHAR(35) NOT NULL,
	last_name VARCHAR(35) NOT NULL,
	email VARCHAR(50) NOT NULL,
	phone VARCHAR(15) NOT NULL
)`

func TestMain(m *testing.M) {
	a = App{}
	a.Initialize()
	log.Print("Initialized")

	tableCreate()

	err := m.Run()

	resetDB()
	os.Exit(err)
}

func TestCreateUser(t *testing.T) {
	resetDB()
	payload := []byte(`{"ID": 1, "first_name": "test", "last_name": "nicholas", "email": "a@a.com",
"phone": "6463151902"}`)
	req, _ := http.NewRequest("POST", "/user",
		bytes.NewBuffer(payload))
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusCreated, resp.Code)
	log.Printf("Create User Error %s", resp.Body.String())
	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)

	if m["first_name"] != "test" {
		t.Errorf("Expected name 'test', instead got %v", m["first_name"])
	}
	if m["last_name"] != "nicholas" {
		t.Errorf("Expect name 'nicholas, got %v", m["last_name"])
	}
	if m["email"] != "a@a.com" {
		t.Errorf("Expect name 'a@a.com', got %v", m["email"])
	}
	if m["phone"] != "6463151902" {
		t.Errorf("Expect name '6463151902, got %v", m["phone"])
	}

}

func TestGetUser(t *testing.T) {
	req, _ := http.NewRequest("GET", "/user/12", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	if m["error"] != "User Not Found" {
		t.Errorf("Expected the 'error' value to be 'User Not Found'. Got '%s'",
			m["error"])
	}
}


func TestUpdateUser(t *testing.T) {
	resetDB()
	addTestUsers(1)

	req, _ := http.NewRequest("GET", "/user/1", nil)
	response := executeRequest(req)
	var initialUser map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &initialUser)

	data := []byte(`{"ID": 1, "first_name": "test", "last_name": "nicholas", "email": "a@a.com",
					"phone": "6463151902"}`)

	req, _ = http.NewRequest("PUT", "/user/1", bytes.NewBuffer(data))
	response = executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	log.Printf("Error: %s", response.Body)

	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)

	if m["first_name"] != "test" {
		t.Errorf("Expected name 'test', instead got %v", m["first_name"])
	}
	if m["last_name"] != "nicholas" {
		t.Errorf("Expect name 'nicholas, got %v", m["last_name"])
	}
	if m["email"] != "a@a.com" {
		t.Errorf("Expect name 'a@a.com', got %v", m["email"])
	}
	if m["phone"] != "6463151902" {
		t.Errorf("Expect name '6463151902, got %v", m["phone"])
	}

}

func TestDeleteUser(t *testing.T) {
	resetDB()
	addTestUsers(1)

	req, _ := http.NewRequest("GET", "/user/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("DELETE", "/user/1", nil)
	response = executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("GET", "/user/1", nil)
	response = executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, response.Code)
}

func TestDownloadUsers(t *testing.T) {
	resetDB()
	addTestUsers(5)
	req, _ := http.NewRequest("GET", "/all_users", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	var m []user
	json.Unmarshal(response.Body.Bytes(), &m)

	f, err := os.Create("./users.csv")
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	w := csv.NewWriter(f)
	for _, obj := range m {
		var record []string
		record = append(record, strconv.Itoa(obj.ID), obj.FirstName,
			obj.LastName, obj.Email, obj.Phone)
		w.Write(record)
	}
	w.Flush()
}

//func TestUploadUsers(t *testing.T) {
//	resetDB()
//	f, err := os.OpenFile("user_upload.csv", os.O_RDONLY, 0666)
//	if err != nil {
//		panic(err)
//	}
//	log.Printf("file opened")
//	//reader := csv.NewReader(bufio.NewReader(f))
//	//var users []user
//	//var u user
//	//for {
//	//	line, err := reader.Read()
//	//	if err == io.EOF {
//	//		break
//	//	} else if err != nil {
//	//		log.Fatal(err)
//	//	}
//	//	u.FirstName = line[0]
//	//	u.LastName = line[1]
//	//	u.Email = line[2]
//	//	u.Phone = line[3]
//	//	u.createUser(a.DB)
//	//	users = append(users, u)
//	//}
//	//uploadedUsers, _ := json.Marshal(users) // bytes.NewBuffer(uploadedUsers)
//
//	req, _ := http.NewRequest("POST", "/upload_users", f )
//	response := executeRequest(req)
//	checkResponseCode(t, http.StatusOK, response.Code)
//
//
//}


func addTestUsers(count int) {
	if count < 1 {
		count = 1
	}

	for i :=0; i < count; i++ {
		statement := fmt.Sprintf("INSERT INTO users(first_name, "+
			"last_name, email, phone) VALUES('%s', '%s', '%s', '%s')",
			"me", "too", "a@.com", "6467771821")
		a.DB.Exec(statement)
	}
}


func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Instead, received %d",
			expected, actual)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder{
	record := httptest.NewRecorder()
	a.Router.ServeHTTP(record, req)
	//log.Printf("#### RECORD %s",record.Body.String())
	return record
}

func tableCreate() {
	if _, err := a.DB.Exec(createTable); err != nil {
		log.Fatal(err)
		log.Print("Fatal")
	} else {
		log.Print("Table Created or Found")
	}
}


func resetDB() {
	a.DB.Exec("DELETE FROM users")
	a.DB.Exec("ALTER SEQUENCE users_id_seq RESTART")
}

