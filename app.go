package main

import (
	"database/sql"
	_ "github.com/lib/pq"

	"github.com/gorilla/mux"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"encoding/json"
	"io"
	"os"
	"encoding/csv"
	"bufio"
)

type App struct {
	Router *mux.Router
	DB *sql.DB
}

func (a *App) Initialize() {
	var err error

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s",
		HOST, PORT, USER, PASSWORD, DB)
	a.DB, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
		log.Printf("error")
	}
	err = a.DB.Ping()
	if err != nil {
		log.Print("#### ERROR")

		panic(err)
	}
	log.Print("Succesfully Pinged")

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}


func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}


func (a *App) getUserController(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil{
		respondWithError(w, http.StatusBadRequest, "Invalid Id")
	}
	u := user{ID: id}
	if err := u.getUser(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "User Not Found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
			}
			return
		}
	respondWithJSON(w, http.StatusOK, u)
}

func (a *App) createUserController(w http.ResponseWriter, r *http.Request) {
	var u user
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
		}
	defer r.Body.Close()
	log.Printf("\n No error While Creating User")
	if err := u.createUser(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}
	respondWithJSON(w, http.StatusCreated, u)

}

func (a *App) updateUserController(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid user ID")
		return
	}

	var u user
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	u.ID = id
	if err := u.updateUser(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusOK, u)
}

func (a *App) deleteUserController(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid User ID")
		return
	}

	u := user{ID: id}
	if err := u.deleteUser(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

func(a *App) downloadUsersCSVController(w http.ResponseWriter, r *http.Request) {
	users, err := getUsers(a.DB)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "No users found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, users)
}

func (a *App) uploadUsersCSVController(w http.ResponseWriter, r *http.Request) {
	//r.Header.Set("Content-Type", "multipart/form-data")
	file, handler, err := r.FormFile("file")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	f, err := os.OpenFile(handler.Filename, os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	reader := csv.NewReader(bufio.NewReader(f))
	io.Copy(f, file)

	var users []user
	var u user
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		if line[0] != "\ufefffirst_name" {
			u.FirstName = line[0]
			u.LastName = line[1]
			u.Email = line[2]
			u.Phone = line[3]
			u.createUser(a.DB)
			users = append(users, u)
		}
		}

	// TODO Return list of json
	// TODO Complete Test for upload`
	// uploadedUsers, _ := json.Marshal(users)
	respondWithJSON(w, http.StatusOK,  map[string]string{"result": "success"})
	log.Print("File Uploaded")
}


func getUsers(db *sql.DB) ([]user, error) {
	var u user
	var users []user
	query := fmt.Sprintf("SELECT id, first_name, last_name, email, phone FROM users")
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email, &u.Phone)
		if err != nil {
			log.Fatal(err)
		}
		users = append(users, u)
	}
	return users, err
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}


func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
