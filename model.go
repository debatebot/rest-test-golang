package main

import (
	"database/sql"
	"fmt"
)

type user struct {
	ID 			int 	`json:"id"`
	FirstName 	string  `json:"first_name"`
	LastName	string	`json:"last_name"`
	Email		string	`json:"email"`
	Phone		string	`json:"phone"`
}

func (u *user) getUser(db *sql.DB) error {
	query := fmt.Sprintf("SELECT first_name, last_name, email, phone FROM users WHERE id=%d", u.ID)
	return db.QueryRow(query).Scan(&u.FirstName, &u.LastName, &u.Email, &u.Phone)
}


func (u *user) createUser(db *sql.DB) error {
	query := fmt.Sprintf("INSERT INTO users(first_name, last_name, email, phone) " +
		"VALUES('%s', '%s', '%s', '%s')", u.FirstName, u.LastName, u.Email, u.Phone)
	_, err := db.Exec(query)
	return err
}

func (u *user) updateUser(db *sql.DB) error {
	query := fmt.Sprintf("UPDATE users SET first_name='%s', last_name='%s', " +
											"email='%s', phone='%s' WHERE id=%d",
											u.FirstName, u.LastName, u.Email, u.Phone, u.ID)
	_, err := db.Exec(query)
	return err
}

func (u *user) deleteUser(db *sql.DB) error {
	query := fmt.Sprintf("DELETE FROM users where id=%d", u.ID)
	_, err := db.Exec(query)
	return err}
