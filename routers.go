package main


func (a *App) initializeRoutes() {
	//a.Router.HandleFunc("/users", a.getUsers).Methods("GET")
	a.Router.HandleFunc("/user/{id:[0-9]+}", a.getUserController).Methods("GET")
	a.Router.HandleFunc("/user", a.createUserController).Methods("POST")
	a.Router.HandleFunc("/user/{id:[0-9]+}", a.updateUserController).Methods("PUT")
	a.Router.HandleFunc("/user/{id:[0-9]+}", a.deleteUserController).Methods("DELETE")
	a.Router.HandleFunc("/all_users", a.downloadUsersCSVController).Methods("GET")
	a.Router.HandleFunc("/upload_users", a.uploadUsersCSVController).Methods("POST")

}
